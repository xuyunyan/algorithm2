package com.algorithm.sorting;

import java.util.Arrays;

public class CountSortChar {
    public static char[] countSortChar(char[] array){
        int[] countArray = new int[256];
        for (int i=0;i<array.length;i++){
            System.out.println((int)array[i]);
            countArray[array[i]]++;
        }
        for (int i=1;i<countArray.length;i++){
            countArray[i]=countArray[i]+countArray[i-1];
        }
        char[] sortedArray = new char[array.length];
        for (int i=array.length-1; i>=0; i--){
            int location = countArray[array[i]];
            countArray[array[i]]--;
            sortedArray[location-1]=array[i];
        }
        return sortedArray;
    }

    public static void main(String[] args) {
        char arr[] = {'g', 'e', 'e', 'k', 's', 'f', 'o', 'r', 'g', 'e', 'e', 'k', 's'};
        char[] sortedArray = countSortChar(arr);
        System.out.println(Arrays.toString(sortedArray));
    }
}
