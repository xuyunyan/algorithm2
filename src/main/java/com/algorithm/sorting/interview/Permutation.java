package com.algorithm.sorting.interview;

import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.LinkedList;

public class Permutation {

    public LinkedList<String> allPermutation(String str) {
        if (str == null || str.length() == 0)
            return null;
        //to save all results
        LinkedList<String> resultList = new LinkedList<String>();
        allPermutation(str.toCharArray(), resultList, 0);
        return resultList;
    }


    private static void allPermutation(char[] c, LinkedList<String> listStr, int start) {


        if (start == c.length - 1)
            listStr.add(String.valueOf(c));
        else {
            for (int i = start; i <= c.length - 1; i++) {
                swap(c, i, start);
                allPermutation(c, listStr, start + 1);
                swap(c, start, i);
            }
        }
    }

    private static void swap(char[] c, int i, int j) {
        char tmp;
        tmp = c[i];
        c[i] = c[j];
        c[j] = tmp;
    }

    private static void print(LinkedList<String> listStr) {
        Collections.sort(listStr);
        for (String str : listStr) {
            System.out.println(str);
        }
        System.out.println("size:" + listStr.size());
    }


    @Test
    public void testNormalCase(){
        LinkedList<String> expectedResult = new LinkedList();
        expectedResult.add("abc");
        expectedResult.add("acb");
        expectedResult.add("bac");
        expectedResult.add("bca");
        expectedResult.add("cba");
        expectedResult.add("cab");
        LinkedList<String> result = allPermutation("abc");
        Assert.assertEquals(expectedResult, result);
        Assert.assertEquals(6,result.size());
    }

    @Test
    public void testNullCase(){
        LinkedList<String> result = allPermutation("");
        Assert.assertEquals(null, result);
    }

}