package com.algorithm.sorting.interview;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PermutationV2 {

    public ArrayList<List<Character>> permutation(List<Character> array){
        if (array.isEmpty()){
            return null;
        }
        ArrayList<List<Character>> resultList = new ArrayList<>();
        if (array.size()==1){
            resultList.add(array);
            return resultList;
        }

        for (Character c : array){
            List<Character> subArray = new ArrayList<>(array);
            subArray.remove(c);
            ArrayList<List<Character>> subResult = permutation(subArray);
            for (List<Character> each : subResult){
                each.add(0, c);
                resultList.add(each);
            }

        }
        return resultList;
    }

    public static void main(String[] args) {
        PermutationV2 permutationV2 = new PermutationV2();
        List<Character> array = new ArrayList<>();
        array.add('a');
        array.add('b');
        array.add('c');
        ArrayList<List<Character>> resultList = permutationV2.permutation(array);
        System.out.println(resultList.toString());
    }

    @Test
    public void testNormalCase(){

        List<Character> array = new ArrayList<>();
        array.add('a');
        array.add('b');
        array.add('c');
        ArrayList<List<Character>> result = permutation(array);
        ArrayList<List<Character>> expectedResult = new ArrayList<>();
        expectedResult.add(Arrays.asList('a','b','c'));
        expectedResult.add(Arrays.asList('a','c','b'));
        expectedResult.add(Arrays.asList('b','a','c'));
        expectedResult.add(Arrays.asList('b','c','a'));
        expectedResult.add(Arrays.asList('c','a','b'));
        expectedResult.add(Arrays.asList('c','b','a'));
        Assert.assertEquals(expectedResult, result);
        Assert.assertEquals(6,result.size());
    }

    @Test
    public void testOneCharacterCase(){

        List<Character> array = new ArrayList<>();
        array.add('a');
        ArrayList<List<Character>> result = permutation(array);
        ArrayList<List<Character>> expectedResult = new ArrayList<>();
        expectedResult.add(Arrays.asList('a'));
        Assert.assertEquals(expectedResult, result);
        Assert.assertEquals(1,result.size());
    }

    @Test
    public void testNullCase(){
        List<Character> array = new ArrayList<>();
        ArrayList<List<Character>> result = permutation(array);
        Assert.assertEquals(null, result);
    }
}
