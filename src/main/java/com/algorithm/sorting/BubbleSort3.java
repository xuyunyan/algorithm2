package com.algorithm.sorting;

import java.util.Arrays;

public class BubbleSort3 {
    /**
     * if the array is already sorted, then stop the loop
     * mark the ordered border, every round, stop at the border
     * @param array
     */
    public static void sort(int array[]){
        int sortBorder = array.length-1;
        for (int i=0; i<array.length-1; i++){
            boolean isSorted = true;
            int tmpSortBorder = 0;
            for (int j=0; j<sortBorder; j++){
                int tmp = 0;
                if (array[j]>array[j+1]){
                    tmp = array[j];
                    array[j] = array[j+1];
                    array[j+1] = tmp;
                    isSorted = false;
                    tmpSortBorder = j;
                }
            }
            sortBorder = tmpSortBorder;
            if (isSorted){
                break;
            }
        }
    }

    public static void main(String[] args) {
        int[] array = new int[]{3,4,2,1,5,6,7,8};
        sort(array);
        System.out.println(Arrays.toString(array));
    }
}
