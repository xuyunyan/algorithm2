package com.algorithm.sorting;

import java.util.Arrays;

public class CocktailSort {

    /**
     * swing right and left
     * @param array
     */
    public static void sort(int array[]){
        int tmp = 0;
        for (int i=0; i<array.length/2; i++){
            boolean isSort = true;
            for (int j=i; j<array.length-i-1; j++){
                if (array[j]<array[j+1]){
                    tmp = array[j];
                    array[j] = array[j+1];
                    array[j+1] = tmp;
                    isSort = false;
                }
            }
            if (isSort){
                break;
            }
            for (int j=array.length-i-1; j>i; j--){
                if (array[j]<array[j-1]){
                    tmp = array[j];
                    array[j] = array[j-1];
                    array[j-1] = tmp;
                    isSort = false;
                }
            }
            if (isSort){
                break;
            }
        }
    }

    public static void main(String[] args) {
        int[] array = new int[]{3,4,2,1,5,6,7,8};
        sort(array);
        System.out.println(Arrays.toString(array));
    }
}
