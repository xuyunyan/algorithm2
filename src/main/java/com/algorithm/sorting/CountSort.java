package com.algorithm.sorting;

import java.util.Arrays;

public class CountSort {

    /**
     * time complexity: O(3n+m)=O(n+m)
     * space complexity: ignore the array, it's O(m)
     * @param array
     * @return
     */
    public static int[] countSort(int[] array){
        //1. get the max value
        // time complexity n
        int max = array[0];
        int min = array[0];
        for (int i=1;i<array.length;i++){
            if (array[i]>max){
                max=array[i];
            }
            if (array[i]<min){
                min=array[i];
            }
        }
        //2. base on length max-min, make an array
        // time complexity n
        int[] countArray = new int[max-min+1];
        for (int i=0; i<array.length; i++){
            int temp = array[i];
            countArray[temp-min]++;
        }
        //3.
        // time complexity m
        for (int i=1; i<countArray.length; i++){
            countArray[i]=countArray[i-1]+countArray[i];
        }

        //4.for loop the array and output result
        // time complexity n
        int[] sortedArray = new int[array.length];
        for (int i=array.length-1;i>=0;i--){
            int location = countArray[array[i]-min];
            sortedArray[location-1]=array[i];
            countArray[array[i]-min]--;
        }
        return sortedArray;
    }

    public static void main(String[] args) {
        int[] array = new int[] {95,94,91,98,99,90,99,93,91,92};
        int[] sortedArray = countSort(array);
        System.out.println(Arrays.toString(sortedArray));
    }
}
