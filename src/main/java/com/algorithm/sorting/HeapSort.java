package com.algorithm.sorting;

import java.util.Arrays;

public class HeapSort {
    /**
     * downAdjust
     * time complexity is O(logn)
     */
    public static void downAdjust(int[] array, int parentIndex, int length){
        //temp for save parentNode
        int temp = array[parentIndex];
        int childIndex = 2 * parentIndex +1;
        while (childIndex < length){
            if (childIndex+1 < length && array[childIndex+1] > array[childIndex]){
                childIndex++;
            }
            if (temp >= array[childIndex]){
                break;
            }
            array[parentIndex] = array[childIndex];
            parentIndex = childIndex;
            childIndex = 2*childIndex + 1;
        }
        array[parentIndex] = temp;
    }

    /**
     * time complexity O(logn)*O(n)=O(nlogn)
     */
    public static void heapSort(int[] array){
        //time complexity is O(logn)
        for (int i = (array.length-2)/2; i>=0; i--){
            downAdjust(array, i, array.length);
        }
        System.out.println(Arrays.toString(array));
        //time complexity is O(n)
        for (int i=array.length-1; i>0; i--){
            int temp = array[i];
            array[i] = array[0];
            array[0] = temp;
            downAdjust(array,0,i);
        }
    }

    public static void main(String[] args) {
        int[] arr = new int[]{1,3,2,6,5,7,8,9,10,0};
        heapSort(arr);
        System.out.println(Arrays.toString(arr));
    }
}
