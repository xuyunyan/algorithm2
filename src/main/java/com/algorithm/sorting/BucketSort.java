package com.algorithm.sorting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;

public class BucketSort {
    public static double[] bucketSort(double[] array){
        //1. get max and min
        double max = array[0];
        double min = array[0];
        for (int i=1; i<array.length; i++){
            if (array[i]>max){
                max=array[i];
            }
            if (array[i]<min){
                min=array[i];
            }
        }
        double d=max-min;
        //2.define bucket
        int bucketNum = array.length;
        ArrayList<LinkedList<Double>> bucketList = new ArrayList<>(bucketNum);
        for (int i=0; i<array.length; i++) {
            bucketList.add(new LinkedList<Double>());
        }
        //3.put the element into bucket
        //区间跨度
        double width = (max-min)/(bucketNum-1); //save the last one for max
        for (int i=0; i<array.length; i++){
            int location = (int) ((array[i]-min)/width);
            bucketList.get(location).add(array[i]);
        }
        //4. sort each bucket
        for (int i=0; i<bucketList.size(); i++){
            Collections.sort(bucketList.get(i));
        }
        //5. output the result
        double[] sortedArray = new double[array.length];
        int index = 0;
        for (LinkedList<Double> linkedList : bucketList){
            for(Double element : linkedList) {
                sortedArray[index++] = element;
            }
        }
        return sortedArray;
    }

    public static void main(String[] args) {
        double[] array = new double[] {4.12,6.421,0.0023,3.0,2.123,8.122,4.12,10.09};
        double[] sortedArray = bucketSort(array);
        System.out.println(Arrays.toString(sortedArray));
    }
}
